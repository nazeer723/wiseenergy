<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model {


public function gettasklist() {
    $query = $this->db->select("*")->from("tasks")->where('isactive','0')->get()->result_array();
    if(count($query)>0) {
        foreach($query as $key => $value) {
            $tasklist[$key]['id'] = $value['id'];
            $tasklist[$key]['task_name'] = $value['task_name'];
            $tasklist[$key]['task_description'] = $value['task_description'];
            $tasklist[$key]['task_start_date'] = $value['task_start_date'];
            $tasklist[$key]['task_end_date'] = $value['task_end_date'];
            $tasklist[$key]['createdat'] = $value['createdat'];
            $tasklist[$key]['updatedat'] = $value['updatedat'];
        }
        $data = $tasklist;
        $code = "103";
        $msg = "Data found";
    } else {
        $data = [];
        $code = "104";
        $msg = "Task list not found";
    }
    return ["code"=>$code, "msg"=>$msg, "data"=>$data];
}

public function createtask($data) {
    $this->db->insert('tasks', $data);
    if($this->db->affected_rows()) {
        $code = "103";
        $msg = "Task Created Successfully";
    } else {
        $code = "104";
        $msg = "Failed to create task";
    }
    return ["code"=>$code, "msg"=>$msg];
}

public function gettaskbyid($data) {
    $id = $data['id'];
    $query = $this->db->select("*")->from("tasks")->where('id',$id)->where('isactive','0')->get()->result_array();
    if(count($query)>0) {
        $data['id'] = $query[0]['id'];
        $data['task_name'] = $query[0]['task_name'];
        $data['task_description'] = $query[0]['task_description'];
        $data['task_start_date'] = date('Y-m-d',strtotime($query[0]['task_start_date']));
        $data['task_end_date'] = date('Y-m-d',strtotime($query[0]['task_end_date']));
        $data['createdat'] = $query[0]['createdat'];
        $data['updatedat'] = $query[0]['updatedat'];
        $code = "103";
        $msg = "Details found";
    } else {
        $data = [];
        $code = "104";
        $msg = "Details not found";
    }
    return ["code"=>$code, "msg"=>$msg, "data"=>$data];
}

public function deletetask($data) {
    $id = $data['id'];
    $query = $this->db->query("update tasks set isactive='1' where id='$id'");
    $code = "103";
    $msg = "Task deleted successfully";
    return ["code"=>$code, "msg"=>$msg];
    // if($query) {
    //     $code = "103";
    //     $msg = "Task deleted successfully";
    // } else {
    //     $code = "104";
    //     $msg = "Failed to delete the task";
    // }
}

public function edittask($data) {
    $id = $data['id'];
    $task_name = $data['task_name'];
    $task_description = $data['task_description'];
    $task_start_date = $data['task_start_date'];
    $task_end_date = $data['task_end_date'];
    $query = $this->db->query("update tasks set task_name='$task_name', task_description='$task_description', task_start_date='$task_start_date', task_end_date='$task_end_date' where id='$id'");
    $code = "103";
    $msg = "Task updated successfully";
    return ["code"=>$code, 'msg'=>$msg];
}



}
?>