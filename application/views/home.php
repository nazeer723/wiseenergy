<?php require "header.php"; ?>
<body>


<div class="container">
	<div class="card">
		<div class="col-md-12">
			<h3 class="text-center mtop20">Task Management System</h3>
		</div>
		<div class="col-xs-12 d-flex flex-row-reverse pad20">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">Create Task</button>
		</div>
		<div class="col-xs-12" id="list" style="height: 80vh; overflow-y:scroll">
			<div class="table-responsive">
    			<table class="table table-bordered" id="tasktable">
			      <thead>
			        <tr>
			          <th>ID</th>
			          <th>Task Name</th>
			          <th>Task Description</th>
			          <th>Start Date</th>
			          <th>End Date</th>
			          <th>Createdat</th>
			          <th>Updatedat</th>
			          <th>Action</th>
			        </tr>
			      </thead>
			      <tbody>
			      	<?php if($tasklist) { 
                		foreach ($tasklist as $task ) { ?>
					        <tr>
					          <td><?php echo $task['id']; ?></td>
					          <td><?php echo $task['task_name']; ?></td>
					          <td><?php echo $task['task_description']; ?></td>
					          <td><?php echo $task['task_start_date']; ?></td>
					          <td><?php echo $task['task_end_date']; ?></td>
					          <td><?php echo $task['createdat']; ?></td>
					          <td><?php echo $task['updatedat']; ?></td>
					          <td width="120px"><button type="button" class="btn btn-primary" onclick="edit_task(<?php echo $task['id']; ?>);"><i class="fas fa-edit"></i></button>&nbsp;<button type="button" class="btn btn-danger" onclick="delete_task(<?php echo $task['id']; ?>);"><i class="fas fa-trash"></i></button></td>
					        </tr>
					    <?php } } else { ?>
					    	<tr>
					    		<td colspan="8" class="text-center">No tasks found</td>
					    	</tr>
					    <?php } ?>
			      </tbody>
			    </table>
			  </div>
            </div>
	</div>

</div>



<!-- The Modal -->
  <div class="modal fade" id="myModal1" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Create Task</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          	<div class="form-group">
		      <label for="task_name">Task Name:</label>
		      <input type="text" class="form-control" id="task_name" name="task_name" placeholder="Enter task name" required>
		    </div>
		    <div class="form-group">
		      <label for="task_description">Task Description:</label>
		      <textarea class="form-control" rows="3" id="task_description" name="task_description" required></textarea>
		    </div>
		    <div class="form-group">
		      <label for="start_date">Start Date:</label>
		      <input type="date" class="form-control" id="start_date" name="start_date" required>
		    </div>
		    <div class="form-group">
		      <label for="end_date">End Date:</label>
		      <input type="date" class="form-control" id="end_date" name="end_date" required>
		    </div>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="create_task();">Submit</button>
        </div>
        
      </div>
    </div>
  </div>


  <!-- The Modal -->
  <div class="modal fade" id="myModal2" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Task</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          	<div class="form-group">
		      <label for="edit_task_name">Task Name:</label>
		      <input type="text" class="form-control" id="edit_task_name" name="edit_task_name" placeholder="Enter task name" required>
		    </div>
		    <div class="form-group">
		      <label for="edit_task_description">Task Description:</label>
		      <textarea class="form-control" rows="3" id="edit_task_description" name="edit_task_description" required></textarea>
		    </div>
		    <div class="form-group">
		      <label for="edit_start_date">Start Date:</label>
		      <input type="date" class="form-control" id="edit_start_date" name="edit_start_date" required>
		    </div>
		    <div class="form-group">
		      <label for="edit_end_date">End Date:</label>
		      <input type="date" class="form-control" id="edit_end_date" name="edit_end_date" required>
		    </div>
		    <input type="hidden" id="edit_id" value="">

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="edittask();">Submit</button>
        </div>
        
      </div>
    </div>
  </div>



</body>
<script>
// $('#myModal1').modal({
//     backdrop: 'static',
//     keyboard: false
// })
$('#myModal1').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
});
$('#myModal2').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
});

function create_task() {
	var task_name = $("#task_name").val();
	var task_description = $("#task_description").val();
	var start_date = $("#start_date").val();
	var end_date = $("#end_date").val();
	if(task_name == "" || task_description == "" || start_date == "" || end_date == "") {
		Swal.fire({
			          title: "Check all the inputs",
			          width: 500,
			          padding: '1em',
			          background: '#fff',
			          backdrop: `
			            rgba(0,0,123,0.4)
			            left top
			            no-repeat
			          `
			        })
	} else {
		$.ajax({
	        type: "POST",
	        url: "<?php echo base_url(); ?>index.php/dashboard/createtask",
	        data: { 
	            task_name : task_name,
	            task_description : task_description,
	            start_date : start_date,
	            end_date : end_date,
	      	},
		    success: function (data) {
		        resultObj = $.parseJSON(data);
		        console.log(resultObj.result);
		        if(resultObj.result == "success") {
		          //$('#alert-resend-success').modal();
		          $('#overlay').fadeOut();
		          $("#myModal1").modal('hide');
		          Swal.fire({
			          title: resultObj.msg,
			          width: 500,
			          padding: '1em',
			          background: '#fff',
			          backdrop: `
			            rgba(0,0,123,0.4)
			            left top
			            no-repeat
			          `
			        })
		          $('#tasktable').load(location.href+" #tasktable>*","");
		        } else {
		          Swal.fire({
			          title: resultObj.msg,
			          width: 500,
			          padding: '1em',
			          background: '#fff',
			          backdrop: `
			            rgba(0,0,123,0.4)
			            left top
			            no-repeat
			          `
			        })
		        }
		   	}
		});
	}
}

function edit_task(id) {
	var id = id;
	if(id == "") {
		Swal.fire({
			          title: "Check all the inputs",
			          width: 500,
			          padding: '1em',
			          background: '#fff',
			          backdrop: `
			            rgba(0,0,123,0.4)
			            left top
			            no-repeat
			          `
			        })
	} else {
		$.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/dashboard/gettaskbyid",
        data: { 
            id:id,
      	},
	    success: function (data) {
	        resultObj = $.parseJSON(data);
	        console.log(resultObj.result);
	        if(resultObj.result == "success") {
	          var data = resultObj.data;
	          console.log(data);
	          $("#edit_task_name").val(data.task_name);
	          $("#edit_task_description").val(data.task_description);
	          $("#edit_start_date").val(data.task_start_date);
	          $("#edit_end_date").val(data.task_end_date);
	          $("#edit_id").val(data.id);
	          $("#myModal2").modal();
	        } else {
	          Swal.fire(
			      'Failed!',
			      'No details found for the task.',
			      'failed'
			    )
	          

	        }
	   	}
	});
	}
}

function edittask() {
	var id = $("#edit_id").val();
	var task_name = $("#edit_task_name").val();
	var task_description = $("#edit_task_description").val();
	var start_date = $("#edit_start_date").val();
	var end_date = $("#edit_end_date").val();
	$.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/dashboard/edittask",
        data: { 
            id:id,
            task_name:task_name,
            task_description:task_description,
            start_date:start_date,
            end_date:end_date,
      	},
	    success: function (data) {
	        resultObj = $.parseJSON(data);
	        console.log(resultObj.result);
	        if(resultObj.result == "success") {
	          //$('#alert-resend-success').modal();
	          $('#overlay').fadeOut();
	          $("#myModal2").modal('hide');
	          Swal.fire(
			      'Updated!',
			      'Your task has been updated.',
			      'success'
			    )
	          $('#tasktable').load(location.href+" #tasktable>*","");
	        } else {
	          Swal.fire(
			      'Failed!',
			      'Failed to update the task.',
			      'failed'
			    )
	          

	        }
	   	}
	});
}

function delete_task(id) {
	var id = id;
	Swal.fire({
	  title: 'Are you sure?',
	  text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!'
	}).then((result) => {
	  if (result.isConfirmed) {
	    deletetask(id);
	  }
	})
}

function deletetask(id) {
	var id = id;
	$.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/dashboard/deletetask",
        data: { 
            id:id,
      	},
	    success: function (data) {
	        resultObj = $.parseJSON(data);
	        console.log(resultObj.result);
	        if(resultObj.result == "success") {
	          //$('#alert-resend-success').modal();
	          $('#overlay').fadeOut();
	          Swal.fire(
			      'Deleted!',
			      'Your task has been deleted.',
			      'success'
			    )
	          $('#tasktable').load(location.href+" #tasktable>*","");
	        } else {
	          Swal.fire(
			      'Failed!',
			      'Failed to delete the task.',
			      'failed'
			    )
	          

	        }
	   	}
	});
}

</script>