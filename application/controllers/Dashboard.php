<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {    
        parent::__construct();
        //session_start();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->helper('directory');
        $this->load->library('upload');
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->helper(array('form', 'url'));
        $this->load->model('dashboard_model');
        
    }

	public function index()
	{
		$res = $this->dashboard_model->gettasklist();
		if($res['code'] == "103") {
			$data['tasklist'] = $res['data'];
		} else {
			$data['tasklist'] = [];
		}
		//print_r($data);
		$this->load->view('home',$data);
	}

	public function createtask() {
		//print_r($_POST);
		$data['task_name'] = $_POST['task_name'];
		$data['task_description'] = $_POST['task_description'];
		$data['task_start_date'] = $_POST['start_date'];
		$data['task_end_date'] = $_POST['end_date'];
		$res = $this->dashboard_model->createtask($data);
		if($res['code'] == "103") {
			echo json_encode(['result'=>'success','msg'=>$res['msg']], 200);
		} else {
			echo json_encode(['result'=>'failed','msg'=>$res['msg']], 200);
		}

	}

	public function gettaskbyid() {
		$data['id'] = $_POST['id'];
		$res = $this->dashboard_model->gettaskbyid($data);
		if($res['code'] == "103") {
			echo json_encode(['result'=>'success','msg'=>$res['msg'], 'data'=>$res['data']], 200);
		} else {
			echo json_encode(['result'=>'failed','msg'=>$res['msg']], 200);
		}
	}

	public function deletetask() {
		//print_r($_POST);
		$data['id'] = $_POST['id'];
		$res = $this->dashboard_model->deletetask($data);
		if($res['code'] == "103") {
			echo json_encode(['result'=>'success','msg'=>$res['msg']], 200);
		} else {
			echo json_encode(['result'=>'failed','msg'=>$res['msg']], 200);
		}

	}

	public function edittask() {
		//print_r($_POST);
		$data['id'] = $_POST['id'];
		$data['task_name'] = $_POST['task_name'];
		$data['task_description'] = $_POST['task_description'];
		$data['task_start_date'] = $_POST['start_date'];
		$data['task_end_date'] = $_POST['end_date'];
		$res = $this->dashboard_model->edittask($data);
		if($res['code'] == "103") {
			echo json_encode(['result'=>'success','msg'=>$res['msg']], 200);
		} else {
			echo json_encode(['result'=>'failed','msg'=>$res['msg']], 200);
		}

	}
}
